namespace Onboard1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addeddataannotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "CustomerName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "CustomerName", c => c.String());
        }
    }
}
