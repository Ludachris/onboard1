﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Onboard1.Models
{
    public class Customers
    {
        [Key]
        public int CustomerId { get; set; }
        [Required (ErrorMessage = "Customer name required")]
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }

        public ICollection<Sales> Sales { get; set; }
            }
}