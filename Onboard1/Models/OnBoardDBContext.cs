﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Onboard1.Models
{
    public class OnBoardDBContext:DbContext
    {
        public OnBoardDBContext() : base("name=Onboard_DB") { }

        public DbSet<Customers> Customers { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<Stores> Stores { get; set; }
        public DbSet<Sales> Sales { get; set; }
    }
}