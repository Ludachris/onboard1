﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace Onboard1.Models
{
    public class Sales
    {
        [Key]
        public int SalesId { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public DateTime DateSold { get; set; }

        public Customers Customers { get; set; }
        public Products Products { get; set; }
        public Stores Stores { get; set; }

    }
}